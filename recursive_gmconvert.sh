#!/bin/zsh

usage() {
	echo >&2 \
	"usage: recursive_gmconvert.sh -f map_filename -t threshold -n num_gaussians -N num_iterations [-i first_iteration] [-s]
Required arguments:
-f map_filename: the file name of the input EM map
-t threshold: the density threshold
-n num_gaussians: number of gaussians per sub-process. Alternatively pass a file which contains a sequence of number of gaussians
-0 num_gaussians0: number of gaussians for first iteration
-N num_iterations: number of recursion levels
Optional arguments:
-i first_iteration: initial recursion level (defaults to 1)
-s: enable serial mode
-h: display this message"
}

#defaults
map_name=
threshold=
ng=
ng0=
N=
i0=1
serial=0

MaxArraySize=100000
get_max_jobs(){
	echo ${MaxArraySize}
}
profile_cmd="time"


while getopts f:t:n:N:i:0:sh opt
do
    case "$opt" in
      f)  map_name="$OPTARG";;
			t)  threshold="$OPTARG";;
			n)  ng="$OPTARG";;
			0)  ng0="$OPTARG";;
			N)  N="$OPTARG";;
			i)  i0="$OPTARG";;
			s)  serial=1;;
			h)  usage
					exit ;;
      \?)	# unknown flag
      	  echo >&2 \
					usage
				  exit 1;;
    esac
done

#check that necessary options were set
if [[ ! -f "$map_name" ]] || [[ -z "$threshold" ]] || [[ -z "$ng" ]]
then
	usage
	exit 1
fi

num_gaussians=()
if [[ -f "$ng" ]]
then
	# ng is a file that contains a list of number of gaussians
	while read n
	do
		num_gaussians+=$n
	done < $ng
else
	# ng is a number and then N needs to be set
	if [[ -z "$N" ]]
	then
		usage
		exit 1
	else
		if [[ -z "$ng0" ]]
		then
			ng0=$ng
		fi

		for ((i=$i0 ; i<=N ; i++))
		do
			if ((i==1))
			then
				num_gaussians+=$ng0
			else
				num_gaussians+=$ng
			fi
		done
	fi
fi


bindir=${0:h}
impdir=~/imp-fast/
gmconvert=${bindir}/gmconvert/gmconvert

init_dir=$PWD
map_root_dir=$(dirname $map_name)
map_name=$(basename $map_name)
cd $map_root_dir


mkdir -p converged
for ((i=$i0 ; i<=${#num_gaussians} ; i++))
do
	n=$i
	ng=${num_gaussians[$i]}
	echo "i=$i n=$n ng=$ng"
	jobname=${map_name}_${n}

	n_jobs=1
	if ((i==1))
	then
		if [[ ! -e threshold.map ]]
		then
			${gmconvert} -imap ${map_name} -zth ${threshold} -oimap threshold.map -ogmm /dev/null -ng 0
	  fi
		echo ${profile_cmd} ${gmconvert} -imap ${map_name} -ogmm $n/${n}_0.gmm -ng ${ng} -zth ${threshold}
		echo ${gmconvert}  -igmm $n/${n}_0.gmm -imap ${map_name} -omap /dev/null
	else
		n_prev=$((i-1))
		gmm_name=${n_prev}/${n_prev}.gmm
		n_jobs=0
		for gmm_file in $(cat ${n_prev}/not_converged.txt)
		do
			n_jobs=$((n_jobs + $(awk '/NGAUSS/{s+=$3}END{print s}' ${gmm_file}) ))
		done
		echo echo ${gmm_name} '$((SLURM_ARRAY_TASK_ID-1+offset))' \> 'tmp_$((SLURM_ARRAY_TASK_ID+offset)).txt'  
		echo ${profile_cmd} ${gmconvert} -imap ${map_name}  -gmml 'tmp_$((SLURM_ARRAY_TASK_ID+offset)).txt' -ogmm $n/${n}_'$((SLURM_ARRAY_TASK_ID-1+offset))'.gmm -ng ${ng} -zth ${threshold}
		echo ${gmconvert}  -igmm $n/${n}_'$((SLURM_ARRAY_TASK_ID-1+offset)).gmm' -imap ${map_name}  -gmml 'tmp_$((SLURM_ARRAY_TASK_ID+offset)).txt' -omap /dev/null
	fi > commands_$i.sh
	mkdir -p $n
	max_jobs=$(get_max_jobs)
	if ((serial==0))
	then
		for ((j=0 ; j<n_jobs ; j=j+max_jobs))
		do
			jobfile=input_${jobname}_${j}.sh
			k=$max_jobs
			if ((j+k > n_jobs))
			then
				k=$((n_jobs - j))
			fi
			echo j=$j, k=$k, jobfile=$jobfile, n_jobs=${n_jobs}
			${bindir}/make_array_from_cmds.sh $jobname $k $j < commands_$i.sh > $jobfile
			date
			sbatch -o ${n}/"slurm-%a_${j}.out" $jobfile
		done
		jobids=$(squeue -O arrayjobid -u $(whoami) -n ${jobname} -S i -h | tr '\n' ':' | tr -d ' ')
		jobids=${jobids%?}
		echo waiting for jobids=$jobids
		#not ok is because gmconvert exits 1
		srun --job-name 'post' --dependency=afternotok:${jobids} --export=ALL,map_name=${map_name},n=${n},bindir=${bindir} --pty ${bindir}/postprocess.sh
	else
		for ((j=1 ; j<=n_jobs ; j++))
		do
			echo $j/$n_jobs
			SLURM_ARRAY_TASK_ID=$j; offset=0; source commands_$i.sh > ${n}/"slurm-${j}_0.out" 2>&1
		done
		source ${bindir}/postprocess.sh 
	fi
	mkdir -p plots
	gnuplot ${bindir}/plot_fsc.gpl > /dev/null 2>&1
	rm -f tmp*
done
cd $init_dir

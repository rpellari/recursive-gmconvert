#!/bin/bash

usage() {
	echo >&2 \
	"usage: get_maps.sh -n num_gaussians -N num_iterations [-s] < input_file.txt
For each emdb entry in input_file.txt, the script downloads it and runs recursive_gmconvert
example: 
get_maps.sh -n 2 -N 2 -s <<< 1883
Required arguments:
input_file.txt should contain one emdb entry per line
-n num_gaussians: number of gaussians per sub-process
-N num_iterations: number of recursion levels
Optional arguments:
-s: enable serial mode
-h: display this message"
}

#defaults
n_gaussians=
N=
serial=0

while getopts n:N:sh opt
do
    case "$opt" in
			n)  n_gaussians="$OPTARG";;
			N)  N="$OPTARG";;
			s)  serial=1;;
			h)  usage
					exit ;;
      \?)	# unknown flag
      	  echo >&2 \
					usage
				  exit 1;;
    esac
done

#check that necessary options were set
if [[ -z "$n_gaussians" ]] || [[ -z "$N" ]]
then
	usage
	exit 1
fi



bindir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
rec_gmconvert=${bindir}/recursive_gmconvert.sh
echo "rec_gmconvert=${rec_gmconvert}"
source ${bindir}/modules.sh

for f in $(cat)
do
	echo $f
	rsync -a -v -z --delete rsync.ebi.ac.uk::pub/databases/emdb/structures/EMD-${f}/ $f
	cd $f
	cutoff=$(tr '<>' ' ' < header/emd-$f.xml | awk '/contour/{print $3}')
	resolution=$(sed -ne 's/[<>/ ]//g' -ne 's/resolutionByAuthor//gp' header/emd-${f}.xml)
	echo ${cutoff} > cutoff.txt
	echo ${resolution} > resolution.txt
	gunzip -c map/emd_${f}.map.gz > emd_${f}.map
	${rec_gmconvert} -f emd_${f}.map -t $cutoff $@
	cd -
done

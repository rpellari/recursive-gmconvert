#!/bin/bash

../../recursive_gmconvert.sh -f emd_3368.map -t $(cat cutoff.txt) -n 4 -N 4 -s

for f in 1 2 3 4
do
	echo $f $(awk '/^#Ngauss/{N=$2} /^CC/{CC=$2; print N,CC}' < $f/gmmscore_threshold)
done > result.txt

result=$(paste result.txt result_check.txt | awk '{checksum+=($1-$4)/$4+($2-$5)/$5+($3-$6)/$6;n+=1} END{print checksum/n>0.01}')
if (( result==0 ))
then
	echo 'PASSED'
else
	echo 'FAILED'
fi
exit $result

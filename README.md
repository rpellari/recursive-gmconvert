# recursive_gmconvert

Implements divide-and-conquer gmm computation as in [1].

## dependencies

EMAN2 (blake.bcm.edu/emanwiki/EMAN2), used to compute the FSC.

gnuplot (www.gnuplot.info/)

## installation

1. clone the repository or download the zip and extract it (we refer to the root of the source tree as `SRC_DIR`).
2. compile gmconvert: `cd ${SRC_DIR}/gmconvert/src ; make ; cd -`
3. if you need to run some commands before executing the scripts, place them in modules.sh

## usage:

```
${SRC_DIR}/recursive_gmconvert.sh -f map_filename -t threshold -n num_gaussians -N num_iterations [-i first_iteration] [-s]
```

### Required arguments:

`-f map_filename`: the file name of the input EM map

`-t threshold`: the density threshold

`-n num_gaussians_or_file`: If the argument is a number, then number of gaussians per sub-process, if argment is a file then read a sequence of number of gaussians from file

`-0 num_gaussians0`: number of gaussians for first iteration

`-N num_iterations`: number of recursion levels

### Optional arguments:

`-i first_iteration`: initial recursion level (defaults to 1)

`-s`: enable serial mode

`-h`: display this message

Unless you enable serial mode (`-s` flag), all the scripts assume that you are running a cluster with the `slurm` queuing system.

### Output:

The script creates a sub-directory (called `i`) for each recursion level `i`, and the output files are called `i/i.gmm` and `i_imp.gmm`.
`i/i.gmm` contains the gmm in `gmconvert` format, and `i_imp.gmm` contains the gmm in `IMP` format (the conversion is handeld by `gmconvert2imp.sh`), which can be read in IMP using `IMP.isd.gmm_tools.decorate_gmm_from_text` function.


## other scripts:

`get_maps.sh` is used to run a batch of `recursive_gmconvert.sh` on a list of emdb entries read from standard input. It will fetch the data from the emdb and run the divide-and-conquer GMM calculation.

Example 1: `get_maps.sh -n 2 -N 2 -s <<< 1883`

Example 2: `get_maps.sh -n 2 -N 2 -s < input.txt`

In example 2, `input.txt` contains one emdb entry per line.

### Arguments:

All arguments from `recursive_gmconvert.sh`, except `-f` and `-t`

## References

[1] Bayesian multi-scale modeling of macromolecular structures based on cryo-electron microscopy density maps

Samuel Hanot, Massimiliano Bonomi, Charles H Greenberg, Andrej Sali, Michael Nilges, Michele Vendruscolo, Riccardo Pellarin

doi: https://doi.org/10.1101/113951

[2] Multiple subunit fitting into a low-resolution density map of a macromolecular complex using a gaussian mixture model. 

Kawabata, T. , Biophys J 2008 Nov 15;95(10):4643-58.

http://www.cell.com/biophysj/fulltext/S0006-3495%2808%2978604-1

[3] EMAN2: an extensible image processing suite for electron microscopy. 

G. Tang, L. Peng, P.R. Baldwin, D.S. Mann, W. Jiang, I. Rees & S.J. Ludtke. 

(2007) J Struct Biol. 157, 38-46. PMID: 16859925 
